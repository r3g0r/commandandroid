package com.example.command;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private ActionManager actionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.edit_text);
        actionManager = new ActionManager();
    }

    public void onInsertTextButtonClick(View view) {
        String textToInsert = "Hello, World!";
        int insertionPosition = editText.getSelectionStart();
        Command insertTextCommand = new InsertTextCommand(editText, textToInsert, insertionPosition);
        actionManager.executeCommand(insertTextCommand);
    }

    public void onDeleteTextButtonClick(View view) {
        int startSelection = editText.getSelectionStart();
        int endSelection = editText.getSelectionEnd();
        Command deleteTextCommand = new DeleteTextCommand(editText, startSelection, endSelection);
        actionManager.executeCommand(deleteTextCommand);
    }

    public void onUndoButtonClick(View view) {
        actionManager.undo();
    }

    public void onRedoButtonClick(View view) {
        actionManager.redo();
    }
}