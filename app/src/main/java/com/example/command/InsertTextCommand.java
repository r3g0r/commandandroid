package com.example.command;

import android.widget.EditText;

public class InsertTextCommand implements  Command {

    private EditText editText;
    private String text;
    private int startPosition;

    public InsertTextCommand(EditText editText, String text, int startPosition) {
        this.editText = editText;
        this.text = text;
        this.startPosition = startPosition;
    }

    @Override
    public void execute() {
        editText.getText().insert(startPosition, text);
    }

    @Override
    public void undo() {
        editText.getText().delete(startPosition, startPosition + text.length());
    }
}
