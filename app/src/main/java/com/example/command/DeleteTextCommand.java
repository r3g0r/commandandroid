package com.example.command;

import android.widget.EditText;

public class DeleteTextCommand implements  Command{

    private EditText editText;
    private String deletedText;
    private int startPosition;

    public DeleteTextCommand(EditText editText, int startPosition, int endPosition) {
        this.editText = editText;
        this.startPosition = startPosition;
        this.deletedText = editText.getText().subSequence(startPosition, endPosition).toString();
    }

    @Override
    public void execute() {
        editText.getText().delete(startPosition, startPosition + deletedText.length());
    }

    @Override
    public void undo() {
        editText.getText().insert(startPosition, deletedText);
    }
}
